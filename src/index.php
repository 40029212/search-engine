
<?php
require_once 'app/init.php';

error_reporting(E_ERROR | E_PARSE);

$searched = false;

if(isset($_GET['q'])&& $_GET['q'] != ''){
  $searched = true;
  $q = $_GET['q'];

$params = [
  'index' => 'websites',
  'body'  => [
      'query' => [
        'bool' => [
          'should' => [
            [ 'multi_match' => [ 'query' => '*'.$q.'*',
            'fields' => ['title', 'url', 'description', 'keywords']] ],
          ]
        ]
      ]
  ]
];
  $query = $es->search($params);

  if($query['hits']['total'] >=1){
    
    $results = $query['hits']['hits'];
    print_r($query['hits']['total']['value']);
  }


  $adsurl = "https://jtld3dwe5l.execute-api.us-east-1.amazonaws.com/default/ad-search?search=".$q;

  try{
  $ads = json_decode(file_get_contents($adsurl), true);

  $adresult = $ads['hits']['hits'];
  }catch(exception $e){

  }
  
  $previousresult;

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    .search-container-outter, search-container-inner
    {
      width:600px;
      position: fixed;
      top: 50%;
      left: 50%;
      margin-top: -100px;
      margin-left: -200px;
    }

    .search-box 
    {
        width: 400px;
        height: 50px;
        font-size:18px;
    }

    .button
    {
      position: fixed;
      top: 50%;
      left: 50%;
      margin-top: -20px;
      margin-left: -20px;
    }

    .results 
    {
      float: left;
      width: 80%;
      height: 100%;

    }

    .ads 
    {
      float: right;

      width: 15%;
      height: 100%;

    }

    .contents 
    {
      top:0%;
      left: 15%;

    }

    .page-title
    {
      font-style: bold;
      font-size: 30px;
    }




</style>
</head>
<body>
  <div class="search">
    <form action="index.php" method = "get" autocomplete = "off">
      <input class="search-box" type="text" placeholder="Enter search here .." name="q">
      <button type="submit" value= "q">Submit</button>
    </form>
  </div>
    <div class="results">
    <?php
    if(isset($results)){
      foreach($results as $r){
        if($r['_source']['url'] == $previousresult['_source']['url']){
          continue;
        }
    ?>
      <div>
        <a target="_blank" class = "page-title" href="<?php echo $r['_source']['url']?>"><?php echo $r['_source']['title']?></a>
      </div>
      <div>
        <label><?php echo $r['_source']['description']?></label>
      </div>

      <hr>
    <?php 
    $previousresult = $r;

      }
    }
    elseif($searched == true && !isset($results) ) {
      ?>
      <label>no results found for : <?php echo $q?>.</label>
    
    <?php
    }

    ?>
    </div>
    <div class = "ads">
    <?php
    if(isset($ads)){
      ?>
    
      <div>
        <label>Things you might like:</label>
    </div>
    <br>
    <br>
      <?php
      foreach($adresult as $ad){
    ?>
        <div>
        <a href="<?php echo $ad['_source']['url']?>"> <?php echo $ad['_source']['url']?></a>
      </div>
      <br>
      <br>
      <br>
    <?php 
      } 
    }
    ?>

  </div>
</body>
</html>
