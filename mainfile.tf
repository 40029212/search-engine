provider "aws" {
  region  = "us-east-1"
}

resource "aws_ecs_service" "php-server-terraform" {
  name            = "terraform-server"
  cluster         = "php-search-server"
  task_definition = "search-php"
  desired_count   = 3
  launch_type     = "FARGATE"
  network_configuration {
      subnets         = ["subnet-f9c41eb4"]
      assign_public_ip = true
  }
}