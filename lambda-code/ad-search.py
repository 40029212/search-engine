import json
import requests

def search(term):
    """Simple Elasticsearch Query"""
    url = "http://ec2-54-159-24-242.compute-1.amazonaws.com:9200/websites/ads/_search?q="+term 
    response = requests.get(url)
    results = json.loads(response.text)
    return results

def lambda_handler(event, context):
    
    return {
        'statusCode': 200,
        'body': json.dumps(search(event['queryStringParameters']["search"]))
    }
